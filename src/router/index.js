import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Ciutadans from  '../components/Ciutadans.vue'
import CiutadaEditar from  '../components/CiutadaEditar.vue'
import CiutadansCrear from  '../components/CiutadansCrear.vue'
import Rebuts from  '../components/Rebuts.vue'
import CodiFont from  '../components/CodiFont.vue'
import Login from  '../components/Login.vue'
import Logout from  '../components/Logout.vue'

import store from '../store'

const routes = [
  {path: '/', name: 'home', component: HomeView},
  {path: '/ciutadanscrear', name: 'ciutadanscrear', component: CiutadansCrear, meta: {requireAuth: true}},
  {path: '/ciutadans', name: 'ciutadans', component: Ciutadans, meta: {requireAuth: true}},
  {path: '/ciutada/:id', name: 'ciutada', component: CiutadaEditar , meta: {requireAuth: true}},
  {path: '/rebuts', name: 'rebuts', component: Rebuts, meta: {requireAuth: true}},
  {path: '/codifont', name: 'codifont', component: CodiFont, meta: {requireAuth: true}},
  {path: '/login', name: 'login', component: Login},
  {path: '/logout', name: 'logout',component: Logout},
 ]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const rutaProtegida = to.matched.some(record => record.meta.requireAuth);
  if(rutaProtegida && store.state.token === null){
      next({name: 'home'})
  }else{
      next()
  }

})

export default router
