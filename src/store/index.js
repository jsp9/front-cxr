import { createStore } from 'vuex'
import router from '../router';

export default createStore({
  state: {
    token: null
  },
  mutations: {
    setToken(state, payload) {
      state.token = payload
    }
  },
  actions: {
    async login({ commit }, usuari) {
      try {
          // const res = await fetch('http://localhost:3002/api/user/login', {
          const res = await fetch(`${process.env.VUE_APP_BASE_URL}/api/user/login`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(usuari)
        })
        const usuariDB = await res.json()
        commit('setToken', usuariDB.data.token)
        localStorage.setItem('token', usuariDB.data.token)
        router.push('/');
      } catch (error) {
        console.log('error: ', error)
        alert('Usuari o paraula de pas incorrectes');
      }
    },
    obtenirToken({ commit }) {
      if (localStorage.getItem('token')) {
        commit('setToken', localStorage.getItem('token'))
      } else {
        commit('setToken', null)
      }
    },
    tancarSessio({ commit }) {
      commit('setToken', null)
      localStorage.removeItem('token')
    }
  }
})

